FROM justb4/mapproxy-mapserver:1.13.2-7.2.2-2

ENV PYTHON_BINDINGS=" \
    autoconf \
    apache2-dev \
    libmapnik-dev \
    mapnik-utils \
    libtool \
    libxml2-dev \ 
    libbz2-dev \
    libgeos-dev \ 
    libgeos++-dev \
    gdal-bin \
    python3 \
    python3-pip \
    python3-mapnik \ 
    python3-psycopg2 \
    python3-yaml"

RUN apt-get update \
    && apt install -y --no-install-recommends \
        $PYTHON_BINDINGS
        
COPY patches/mapnik.py /usr/local/lib/python3.11/site-packages/mapproxy/source/mapnik.py
COPY patches/geopackage.py /usr/local/lib/python3.11/site-packages/mapproxy/cache/geopackage.py
