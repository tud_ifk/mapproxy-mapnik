#!/usr/bin/env bash
set -e

version_var=$(sed -n "s#__version__ =\s*'\(.*\)'#\1#p" version/version.py)

# push
docker push registry.gitlab.vgiscience.org/tud_ifk/mapproxy-mapnik:$version_var