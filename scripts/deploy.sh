#!/usr/bin/env bash

# Exit in case of error
set -e

ci_job_token=${ci_job_token}
source ./scripts/build.sh
source ./scripts/push.sh