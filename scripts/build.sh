#!/usr/bin/env bash

# Exit in case of error
set -e

version_var=$(sed -n "s#__version__ =\s*'\(.*\)'#\1#p" version/version.py)

docker build --build-arg CI_JOB_TOKEN=$ci_job_token -t registry.gitlab.vgiscience.org/tud_ifk/mapproxy-mapnik:$version_var .
