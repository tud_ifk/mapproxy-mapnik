[![Build status](https://gitlab.vgiscience.de/tud_ifk/mapproxy-mapnik/badges/master/pipeline.svg)](https://gitlab.vgiscience.de/tud_ifk/mapproxy-mapnik/commits/master)

## Container Build for Gitlab Registry Image "mapproxy-mapnik"

This repository is used to build the gitlab registry image "mapproxy-mapnik", 
which contains mapproxy base docker image plus Mapnik.

Built container `Dockerfile` is automatically pushed to registry using sh files in `./scipts/`.

## Setup

To create a new release, after changing the Dockerfile, update the file `version/version.py` manually and push to remote.

## Manual

```
docker build .
docker tag sha256:5b207603fab201c4266729e33624f18eb903d1fcc8054a025484e30e1412a136 registry.gitlab.vgiscience.org/tud_ifk/mapproxy-mapnik:0.1.0
docker push registry.gitlab.vgiscience.org/tud_ifk/mapproxy-mapnik:0.1.0
```

## License

This project is licensed under the terms of the MIT license.
